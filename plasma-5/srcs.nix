{ fetchgit }:
{
  bluedevil = {
    version = "5.22.80";
    name = "bluedevil-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/bluedevil.git";
      rev = "faade374f5462cf5ddd99b4561d72ee47cc0fdef";
      sha256 = "0idfcbxbv97hi1wipm9ig43qdam7qgamp468iniyxxr0xy9ndk32";
    };
  };
  breeze-grub = {
    version = "5.22.80";
    name = "breeze-grub-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/breeze-grub.git";
      rev = "63136a9d4efe3e0fb5638338bfc8660157d80c08";
      sha256 = "0g9bbk23m162h5igz69syjmwk9973fi16iijmb2596wd7d1xaxk5";
    };
  };
  breeze-gtk = {
    version = "5.22.80";
    name = "breeze-gtk-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/breeze-gtk.git";
      rev = "fccd7df4c3c86f587b595b8f8149fa403858bc4d";
      sha256 = "0kginqd5kxssv04h011f6ig9ypm9gfywf2949dsgphxvmrwbp9nh";
    };
  };
  breeze-plymouth = {
    version = "5.22.80";
    name = "breeze-plymouth-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/breeze-plymouth.git";
      rev = "8ed816b9c4f60c05a2f7646c30a24b69700006ff";
      sha256 = "17b4s1aqwjmh9hs9j53w2ksa2vfr751kzvigh1c7a5352099kmgl";
    };
  };
  breeze-qt5 = {
    version = "5.22.80";
    name = "breeze-qt5-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/breeze.git";
      rev = "162767a661aaacb213081f303c90b802ac16c908";
      sha256 = "12pvjsxrs3j237dxh6i3ghhwvqsa31ks0bwcx737977icbapgk9p";
    };
  };
  discover = {
    version = "5.22.80";
    name = "discover-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/discover.git";
      rev = "656641785f45b1a6cd700643e2446dd4fc252b76";
      sha256 = "1m9hx0ib0a6aihj7z5nx56x4d93a8d0j02zm8671iay2m2d180f9";
    };
  };
  kactivitymanagerd = {
    version = "5.22.80";
    name = "kactivitymanagerd-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kactivitymanagerd.git";
      rev = "9cabb88c59794d910699e02ea3c525d3c0b40c63";
      sha256 = "195c4w1zxn4gjvsfx1q3x13csfvlr82ch9p387vlxbnr3x4669b1";
    };
  };
  kde-cli-tools = {
    version = "5.22.80";
    name = "kde-cli-tools-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kde-cli-tools.git";
      rev = "ddccf63b763129cc40c7f5bab4f03302dfdc9860";
      sha256 = "1qbkmn4jan1ksgw1677fpd2s5rjvy5br85cbs7l3ayw4l72j9f3f";
    };
  };
  kde-gtk-config = {
    version = "5.22.80";
    name = "kde-gtk-config-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kde-gtk-config.git";
      rev = "93da65acd61c8605ac15e9f019b1898ae35fd287";
      sha256 = "03gipmzp45nfman6pld7jfnx7chmpmlsyvfsnrz7vxxa71ggiyk0";
    };
  };
  kdecoration = {
    version = "5.22.80";
    name = "kdecoration-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kdecoration.git";
      rev = "1dabf572c4de84ef6c798ddc2d4edc38fec9dd85";
      sha256 = "0n78y7993y3y3198i58rsc1mcf9n8l53iyby6qxa0mkbwxg4vf3q";
    };
  };
  kdeplasma-addons = {
    version = "5.22.80";
    name = "kdeplasma-addons-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kdeplasma-addons.git";
      rev = "f600a856fda735476694c52eaff5ed7c35e6cba2";
      sha256 = "1v1rpziycrvafxjwlk199f88rv7g2cs4bzy28kvfz4barhwcxm70";
    };
  };
  kgamma5 = {
    version = "5.22.80";
    name = "kgamma5-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kgamma5.git";
      rev = "5885d2fc3407393a583b87a39f18e800b5c30d14";
      sha256 = "1b6kffckw7zdhvgyrmdhnkadjshnb3fjzig9xvgxx8h3x9ka14ns";
    };
  };
  khotkeys = {
    version = "5.22.80";
    name = "khotkeys-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/khotkeys.git";
      rev = "caad4c148b87641c296042b980a06c935257782d";
      sha256 = "085kgmmq6dqhql50a8p1r0mi58da3wk5zvmxp9f4bsby701csq3x";
    };
  };
  kinfocenter = {
    version = "5.22.80";
    name = "kinfocenter-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kinfocenter.git";
      rev = "07fdf4ac7fb4f812ab60e04df287efdd4464905c";
      sha256 = "15wy50m8p4ynnsjhsqv98sf4rfl2ywz1hwynmz2gch80jazi0rkx";
    };
  };
  kmenuedit = {
    version = "5.22.80";
    name = "kmenuedit-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kmenuedit.git";
      rev = "27420037f212860db4280d349c17b961c4b11f21";
      sha256 = "1flnbzxc0qvzqq92pap97fzash900wb493xqmp0jdisasc3qssgi";
    };
  };
  kscreen = {
    version = "5.22.80";
    name = "kscreen-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kscreen.git";
      rev = "413adfe1dcd233de9865abed51c9a409dee00c20";
      sha256 = "1dyn27f7bzv3fgbfmbk1ji7y10r85xsx11i1355cwnrw87h8y2xn";
    };
  };
  kscreenlocker = {
    version = "5.22.80";
    name = "kscreenlocker-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kscreenlocker.git";
      rev = "ad1fa6414d6ed9b413b597e7fe2b2d29427fea5f";
      sha256 = "14zcszhy4jhy7waj8c0xf7jy31cwx01q4nw2mh3x9l8ic6fq1k1r";
    };
  };
  ksshaskpass = {
    version = "5.22.80";
    name = "ksshaskpass-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/ksshaskpass.git";
      rev = "daa26793a85bbd4619bbbb464d9198533f732c67";
      sha256 = "094a429fgf79z1qhvijpaww71jdamdy82c5rlcbypq35awb5zxjc";
    };
  };
  ksysguard = {
    version = "5.22.80";
    name = "ksysguard-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/ksysguard.git";
      rev = "9bc6de877110dee2c4382eb1931a278bf7fbcc02";
      sha256 = "1m0aqfj2ir9wiz7li8bkfzx0xcj6978ck3ng3vigni5p84b0h7si";
    };
  };
  ksystemstats = {
    version = "5.22.80";
    name = "ksystemstats-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/ksystemstats.git";
      rev = "5f93a3fc63dd35c68c15289c6293e5cde6a4c68c";
      sha256 = "0mahg3wpd5cwcrm8csv6ananjy2ji6mdzd7l3q7cbs1w3227gy85";
    };
  };
  kwallet-pam = {
    version = "5.22.80";
    name = "kwallet-pam-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kwallet-pam.git";
      rev = "3bdf94ab71e4d8845d447e1b24b77234a1a48a83";
      sha256 = "1hj08n3c87bvcgh99pqjph3xndd8qh2jd2b623bbrqkqmk5gg3gw";
    };
  };
  kwayland-integration = {
    version = "5.22.80";
    name = "kwayland-integration-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kwayland-integration.git";
      rev = "590151227f46f2414781309ceecc68bde9c1d548";
      sha256 = "1lxhv8h5nh9wrdhib4bl19agizcs2hyd84j7g21nzxcmyzyzsl7r";
    };
  };
  kwayland-server = {
    version = "5.22.80";
    name = "kwayland-server-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kwayland-server.git";
      rev = "37890904e9a19892faa1ab896f52e4b48625b251";
      sha256 = "0l3ad6nyzgmiaqxvnb3zrhn3i7c98xc0mxsnqa5b18a0yx8gpd1z";
    };
  };
  kwin = {
    version = "5.22.80";
    name = "kwin-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kwin.git";
      rev = "9e5cb836a3459e127c87688fb1f2f8aefc95f170";
      sha256 = "15jpzyy70m5i506nx5l2xirkp468s2xx03f3nngl6l5qacmn976w";
    };
  };
  kwrited = {
    version = "5.22.80";
    name = "kwrited-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/kwrited.git";
      rev = "8d34f293ddfb7890d15966f46ed510cb85741914";
      sha256 = "1nbnv4hjns5y8gwkxhhf9nhq83vlyg1kd8386k4ivrr9rz7r3kd7";
    };
  };
  layer-shell-qt = {
    version = "5.22.80";
    name = "layer-shell-qt-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/layer-shell-qt.git";
      rev = "f6084b2c438a9f238f8245262daa36780d3f0947";
      sha256 = "1wzy5mkcf1nsncbf1b9k38274qqb54yc45y3yai6bkf5574gchj2";
    };
  };
  libkscreen = {
    version = "5.22.80";
    name = "libkscreen-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/libkscreen.git";
      rev = "19fb52b398b81a9392f10c9bca5499817a542dcd";
      sha256 = "03y1yy2ipbxms4lvf5i9i1qbxq77nvxwh5kid83dgp2lxva2i7gy";
    };
  };
  libksysguard = {
    version = "5.22.80";
    name = "libksysguard-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/libksysguard.git";
      rev = "375ee5b2a66b7fc0818857210e5af63e025974a4";
      sha256 = "1rrchlba57h0k7ymmdb3nhid7qgcjlz7cxnghb1wp57pk20gxlj9";
    };
  };
  milou = {
    version = "5.22.80";
    name = "milou-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/milou.git";
      rev = "65c72f4b498ff3cd023350383568662e01603ff8";
      sha256 = "13ffrlxzvmblj4w5qwwpwybkk2kp67sn9mp4wxjwhjj5v2dxnckn";
    };
  };
  oxygen = {
    version = "5.22.80";
    name = "oxygen-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/oxygen.git";
      rev = "5c8c3271604320c540a58104370b602352759ac0";
      sha256 = "0ylasgmr0z3q2rp9jcskpcpdj3mdqaiwqdhb1grjb8gifw7ks0qg";
    };
  };
  plasma-browser-integration = {
    version = "5.22.80";
    name = "plasma-browser-integration-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-browser-integration.git";
      rev = "b9e54d8c61224322ebbda2bb5dc2a78b50dda847";
      sha256 = "04k2z9mbxhwb65915zn9jnx7sr2yyanyi1aym4hni7vc6335mbhy";
    };
  };
  plasma-desktop = {
    version = "5.22.80";
    name = "plasma-desktop-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-desktop.git";
      rev = "1afe75a8b2e1f3f37bcd5bdcc2cdc1bf29779453";
      sha256 = "0pkvs87jnpz5wnxysqcqws5j7370z8i9h2cnjlcnpr10m5hxiwkg";
    };
  };
  plasma-disks = {
    version = "5.22.80";
    name = "plasma-disks-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-disks.git";
      rev = "aaf1ef64b5e4b19844736c42bb80806c207616b7";
      sha256 = "05hpjn9mfh7zbibs1llzpssranbi8k9l2if0y04vc97lz6ihi4hi";
    };
  };
  plasma-integration = {
    version = "5.22.80";
    name = "plasma-integration-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-integration.git";
      rev = "f7c25d9c87c6096889d0a687ca2724bebeddd518";
      sha256 = "1k3gd96f56sj7rlg0c2hrjrcq2f5n2x2vwy5g40bk4pbx3ncq03h";
    };
  };
  plasma-nm = {
    version = "5.22.80";
    name = "plasma-nm-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-nm.git";
      rev = "f891c384d9ed5edd25965c57637ddbf2611d029b";
      sha256 = "1zklrrvappxx4iisdrav8izkydbrc3q05bdbgqqk083p1w330xqf";
    };
  };
  plasma-pa = {
    version = "5.22.80";
    name = "plasma-pa-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-pa.git";
      rev = "e87dcf336bf7e3649b6ce4dc124714347436d39e";
      sha256 = "03i6y6bdqjdnrsk3v9grzg1mva8asp6rihqa0964sml705j7qx15";
    };
  };
  plasma-sdk = {
    version = "5.22.80";
    name = "plasma-sdk-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-sdk.git";
      rev = "32b423a681643700a72ca0c31473b83c6995c286";
      sha256 = "0h65z4gh8aby4cbq2bf5zx5gv2qh2gi71gxgdg0imny781pjjl51";
    };
  };
  plasma-systemmonitor = {
    version = "5.22.80";
    name = "plasma-systemmonitor-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-systemmonitor.git";
      rev = "c3fb6e0f0579aa8a228fa787d4502a5350d48675";
      sha256 = "1ayyyrz3sw9v00144nijjls4h9z5ki51rakz2dnyil7lsf4k9q69";
    };
  };
  plasma-thunderbolt = {
    version = "5.22.80";
    name = "plasma-thunderbolt-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-thunderbolt.git";
      rev = "ff4b4f95932f0e6dbc0178939ab5780f3a3c5e02";
      sha256 = "05fd83mgz8hmlz5pps6n2p444kf5z9xv2sjm061v60vnlm7ph7yv";
    };
  };
  plasma-vault = {
    version = "5.22.80";
    name = "plasma-vault-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-vault.git";
      rev = "2b4911828921b86cffa84999b26079181106fc9a";
      sha256 = "0vhjp4a28yaq5jxr3gq3nc2i7sd4g9b0f5mf4g3zziwy04q2b4v4";
    };
  };
  plasma-workspace = {
    version = "5.22.80";
    name = "plasma-workspace-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-workspace.git";
      rev = "27704daf4440b1d41262d687bf60b734fbc8475c";
      sha256 = "178gh2qifaaksdq8m7kaigw4i57j168ck7fb6shrmz576bipkzvq";
    };
  };
  plasma-workspace-wallpapers = {
    version = "5.22.80";
    name = "plasma-workspace-wallpapers-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/plasma-workspace-wallpapers.git";
      rev = "e46229920f98149cd0856f1427f039e8718bf711";
      sha256 = "01grpb2b1jn92aim3w9zrbhbmpf5s154brdp8p77z6rsrv1pfca6";
    };
  };
  polkit-kde-agent = {
    version = "5.22.80";
    name = "polkit-kde-agent-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/polkit-kde-agent-1.git";
      rev = "12157d1e690735ca8ce22e9c044943ab41308a69";
      sha256 = "0w3g2b4nf2f31b212w34zlqlamvfscjdwv12r4hyqch7rqz4l7qs";
    };
  };
  powerdevil = {
    version = "5.22.80";
    name = "powerdevil-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/powerdevil.git";
      rev = "93e441d9a3e83bee9fcc4bc91fc9f7600af818c5";
      sha256 = "151wm8c922yskcwlknbqk8grkgj0ibp8b73844hxjdaf59n79g43";
    };
  };
  qqc2-breeze-style = {
    version = "5.22.80";
    name = "qqc2-breeze-style-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/qqc2-breeze-style.git";
      rev = "dcfd65fd5a2fb5d1cc74c961b26e46fcb35cb5f3";
      sha256 = "17nwmigsxrn988rqy34nyy49nvr4r1dbb7w92lzzzqk8c939wa6j";
    };
  };
  sddm-kcm = {
    version = "5.22.80";
    name = "sddm-kcm-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/sddm-kcm.git";
      rev = "c51c2a1d9c80caf6bc98bc7fcf8970fa6c4d0719";
      sha256 = "1h9b46ix45wfy4zwwlx936zm4vl38x5v4d5dvch0k57jhbb7kfqn";
    };
  };
  systemsettings = {
    version = "5.22.80";
    name = "systemsettings-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/systemsettings.git";
      rev = "07fcc030329d9820e76007be5e067463542507e5";
      sha256 = "051anzx56zzifiqhc9qywq36xjq3hyfwgr4wwifnmvdwisx1ffai";
    };
  };
  xdg-desktop-portal-kde = {
    version = "5.22.80";
    name = "xdg-desktop-portal-kde-5.22.80";
    src = fetchgit {
      url = "https://invent.kde.org/plasma/xdg-desktop-portal-kde.git";
      rev = "96ead889d8960cde0e2f0628dcda9ccbc0d8e3fb";
      sha256 = "1pnrv8lx394720zpsprsh55y3mk1x7p98kaqyzra6p7cp07lskgw";
    };
  };
}