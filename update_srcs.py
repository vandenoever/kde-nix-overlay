#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p python3 nix-prefetch-git python3Packages.aiohttp

import os
import json
import asyncio
import aiohttp
from glob import glob
from types import SimpleNamespace
from typing import List, Dict, Optional, TypedDict, Tuple


def get_group(
    groups: List[str], specialSlugs: Dict[str, str], name: str
) -> Optional[str]:
    slug = name
    if name in specialSlugs:
        slug = specialSlugs[name]
    for group in groups:
        if slug in projects[group]:
            return group
    print(f"No group for {slug}")
    return None


def is_project(groups: List[str], specialSlugs: Dict[str, str], name: str) -> bool:
    return get_group(groups, specialSlugs, name) is not None


def list_packages(
    dir: str, groups: List[str], specialSlugs: Dict[str, str]
) -> List[str]:
    names: List[str] = []
    for file in glob(dir + "/*.nix"):
        name = os.path.splitext(os.path.basename(file))[0]
        if is_project(groups, specialSlugs, name):
            names.append(name)
    for file in glob(dir + "/*/default.nix"):
        name = os.path.basename(os.path.dirname(file))
        if is_project(groups, specialSlugs, name):
            names.append(name)
    names.sort()
    return names


class GitInfo:
    url: str
    rev: str
    sha256: str


def nix_prefetch_git(url: str) -> GitInfo:
    pipe = os.popen("nix-prefetch-git --quiet " + url)
    output = pipe.read()
    exit_code = pipe.close()
    if exit_code:
        exit(exit_code)
    # convert json into a python object
    git_info = json.loads(output, object_hook=lambda d: SimpleNamespace(**d))
    return git_info


def create_srcs_nix(
    groups: List[str], version: str, names: List[str], specialSlugs: Dict[str, str]
) -> str:
    output = """{ fetchgit }:
{"""
    for name in names:
        slug = name
        if name in specialSlugs:
            slug = specialSlugs[name]
        gi = None
        group = get_group(groups, specialSlugs, name)
        if group is None:
            print(f'No group for "{name}"')
            continue
        print(f"\t{name}\t{group}/{slug}")
        url = f"https://invent.kde.org/{group}/{slug}.git"
        gi = nix_prefetch_git(url)
        output += f"""
  {name} = {{
    version = "{version}";
    name = "{name}-{version}";
    src = fetchgit {{
      url = "{gi.url}";
      rev = "{gi.rev}";
      sha256 = "{gi.sha256}";
    }};
  }};"""
    output += "\n}"
    return output


class ConfigItem(TypedDict):
    dir: str
    groups: List[str]
    version: str
    specialSlugs: Dict[str, str]


Config = List[ConfigItem]


def create_srcs(config: ConfigItem):
    dir = config["dir"]
    print(dir)
    groups = config["groups"]
    specialSlugs = config["specialSlugs"]
    version = config["version"]
    names = list_packages(dir, groups, specialSlugs)
    srcs = create_srcs_nix(groups, version, names, specialSlugs)
    with open(dir + "/srcs.nix", "w") as f:
        f.write(srcs)


async def get_group_projects(session, group: str) -> Tuple[str, List[str]]:
    url = f"https://invent.kde.org/api/v4/groups/{group}"
    response = await session.get(url)
    json = await response.json()
    projects = [p["path"] for p in json["projects"]]
    projects.sort()
    return (group, projects)


async def get_groups(config: Config) -> Dict[str, List[str]]:
    group_names = {i for c in config for i in c["groups"]}
    async with aiohttp.ClientSession() as session:
        tasks = [get_group_projects(session, g) for g in group_names]
        return dict(await asyncio.gather(*tasks))


config: Config = [
    {
        "dir": "kde-frameworks",
        "groups": ["frameworks"],
        "version": "5.84.0",
        "specialSlugs": {"kirigami2": "kirigami"},
    },
    {
        "dir": "plasma-5",
        "groups": ["plasma"],
        "version": "5.22.80",
        "specialSlugs": {
            "breeze-qt5": "breeze",
            "polkit-kde-agent": "polkit-kde-agent-1",
        },
    },
    {
        "dir": "kde",
        "groups": [
            "accessibility",
            "education",
            "games",
            "graphics",
            "libraries",
            "multimedia",
            "network",
            "pim",
            "sdk",
            "system",
            "utilities",
        ],
        "version": "21.04.3",
        "specialSlugs": {},
    },
]

projects = asyncio.run(get_groups(config))
for c in config:
    create_srcs(c)
