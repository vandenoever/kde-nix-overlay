[(final: prev:
  let
    libsForQt5 = prev.libsForQt5.overrideScope' (
      selfx: prevx:
        let
          kdeFrameworks = prevx.kdeFrameworks.overrideScope' (
            selfy: prevy: (import ./kde-frameworks) {
              inherit (prev) lib fetchurl fetchgit;
              inherit (final) libsForQt5;
            }
          );
          plasma5 = prevx.plasma5.overrideScope' (
            selfy: prevy: (import ./plasma-5) {
              inherit (prev) lib fetchgit gsettings-desktop-schemas;
              inherit (final) libsForQt5;
              gconf = prev.gnome2.GConf;
            }
          );
          kdeGear = prevx.kdeGear.overrideScope' (
            selfy: prevy: (import ./kde) {
              inherit (prev) lib fetchgit;
              inherit (final) libsForQt5;
            }
          );
          plasma-wayland-protocols = prevx.plasma-wayland-protocols.overrideAttrs ( old: rec {
            pname = "plasma-wayland-protocols";
            version = "1.3.0";
            src = prev.fetchurl {
              url = "mirror://kde/stable/${pname}/${pname}-${version}.tar.xz";
              sha256 = "DaojYvLg0V954OAG6NfxkI6I43tcUgi0DJyw1NbcqbU=";
            };
          });
          all = kdeFrameworks // plasma5 // plasma5.thirdParty // kdeGear;
          libsForQt5 = all // {
            inherit kdeFrameworks plasma5 kdeGear plasma-wayland-protocols;
            kdeApplications = kdeGear;
          };
        in libsForQt5 // {
          inherit libsForQt5;
        });
  in {
    inherit libsForQt5;
    inherit (libsForQt5) plasma-desktop;
    plasma5Packages = libsForQt5;
  }
)]
