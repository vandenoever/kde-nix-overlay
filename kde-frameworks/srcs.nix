{ fetchgit }:
{
  attica = {
    version = "5.84.0";
    name = "attica-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/attica.git";
      rev = "328f2d9213bc79799c58ee48c5e05e2be0c12d75";
      sha256 = "1h9kgfzqhbjs2l6lw73rimaaz51wf57j4bajzmb5q48m0bm414p6";
    };
  };
  baloo = {
    version = "5.84.0";
    name = "baloo-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/baloo.git";
      rev = "c28a53c60100cfe251cc8914925e7a3c435d7b4b";
      sha256 = "1nn78qy1m7ijdr2m68vdgk6cz2pch2afr2njrdqfwhnr17qb5am9";
    };
  };
  bluez-qt = {
    version = "5.84.0";
    name = "bluez-qt-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/bluez-qt.git";
      rev = "ba48b662f7d5cf59f8f839e19642e95714a1ef6b";
      sha256 = "03snkhks3c21dkbcsarch86vlrzxjii9kcwhl49ws5z88cqrz58a";
    };
  };
  breeze-icons = {
    version = "5.84.0";
    name = "breeze-icons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/breeze-icons.git";
      rev = "c6fad5c9b2232108dd2011284260fbd88fea1bfd";
      sha256 = "0llid8v3przqdl6msljyn5h6jsnf83w67x5w6x93qmn50fbfpdhb";
    };
  };
  extra-cmake-modules = {
    version = "5.84.0";
    name = "extra-cmake-modules-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/extra-cmake-modules.git";
      rev = "95d1808924d9ecdf3037939ba78950fe20ee1396";
      sha256 = "0nhdkfa9hriqwrzfyrddigb54gn84743hvlgskc1bscivrr4n9z0";
    };
  };
  frameworkintegration = {
    version = "5.84.0";
    name = "frameworkintegration-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/frameworkintegration.git";
      rev = "4aa5e780a4a5c5024317ff6ece72765f65d46368";
      sha256 = "197r10vyckkzgnh4ig0x98iz1fhxq3bn8p2v5xa2k0chz5w6nmz0";
    };
  };
  kactivities = {
    version = "5.84.0";
    name = "kactivities-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kactivities.git";
      rev = "03eacefa1403f0ada3da884593b4bc6f5ae1eee5";
      sha256 = "1brraz57zdaz2kfzg7yakgvnbhdr3h7c5pgl8hx0dk8dlrd4kbk9";
    };
  };
  kactivities-stats = {
    version = "5.84.0";
    name = "kactivities-stats-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kactivities-stats.git";
      rev = "80c6b1d2ae3fb7d661b055b2ebc999f04e8cdb37";
      sha256 = "1akglbx55qpz9q0jlzx7xbhzj1ydkpr3bk6bpmd7cajskbj8lz5a";
    };
  };
  kapidox = {
    version = "5.84.0";
    name = "kapidox-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kapidox.git";
      rev = "0326b9026f2cd280ad264e6f2b4ce623daec167c";
      sha256 = "015pprnb739i2azh76d2yzm1fn0gwz6jvgcrk3xzjz2vs3gs0nns";
    };
  };
  karchive = {
    version = "5.84.0";
    name = "karchive-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/karchive.git";
      rev = "e657605a346cd0934c643f54efa82711acc4d353";
      sha256 = "0s2ijdf8j7262bgbf6av5g6s4ymnc8cyx765ss09j34xf27iipvz";
    };
  };
  kauth = {
    version = "5.84.0";
    name = "kauth-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kauth.git";
      rev = "ef65bd6efa78f27cbc3a34b88a7c3e59be488864";
      sha256 = "1rls3gv4kraickym0zh5yx61v21jnfn98l08sm855v5mb94v6h5p";
    };
  };
  kbookmarks = {
    version = "5.84.0";
    name = "kbookmarks-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kbookmarks.git";
      rev = "80d61ce11cc263f198cb6dc0ea35111c069ba1c0";
      sha256 = "0lc1bckimi04si7y29m6ljhhn6dyzdipkj0cc7rv7fajp2nw4qzm";
    };
  };
  kcalendarcore = {
    version = "5.84.0";
    name = "kcalendarcore-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcalendarcore.git";
      rev = "db39bb52cedce07684436c775415b77179f35a76";
      sha256 = "1i4piynl5zja6m5lmhbl6ilhmizkvinrgbvvrdqhsz99zv2qiy21";
    };
  };
  kcmutils = {
    version = "5.84.0";
    name = "kcmutils-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcmutils.git";
      rev = "a362ddf892f8e84affe73da8b6b5bacf0655dafd";
      sha256 = "04lv128ilfmybgh7l7yil0a7aw7446gk1kp3ivwc18nwdqpfqd48";
    };
  };
  kcodecs = {
    version = "5.84.0";
    name = "kcodecs-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcodecs.git";
      rev = "c58a80e863fa10a933bbc3c1ca66c63d74f8ca8f";
      sha256 = "0mcnm1jbwrk76aih8ky4h7wsbz03vp6f0as69q35zq4l321nd7kr";
    };
  };
  kcompletion = {
    version = "5.84.0";
    name = "kcompletion-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcompletion.git";
      rev = "7a0b9d2c54e4f83309ef81b9d1c14aa38197e334";
      sha256 = "180z8x2pfxbypdpr2nps63ih40bz7h1nhv5hjb93rl0kv5ni8rbk";
    };
  };
  kconfig = {
    version = "5.84.0";
    name = "kconfig-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kconfig.git";
      rev = "dcc5f6529d3008f2cf3a678a4c42d5092b406690";
      sha256 = "1i2hdbw809dsinn1xz2mv43xffvyw9qrvi1h0kdg6bj8sr6wmnjw";
    };
  };
  kconfigwidgets = {
    version = "5.84.0";
    name = "kconfigwidgets-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kconfigwidgets.git";
      rev = "83a1edfce9ca1e3553cf81e2b93dd62b9b2d8c11";
      sha256 = "0vrfnlva1y76w3dnh0kw2l9akcgqh9lkd5svlnfpc03q9bbrxqkd";
    };
  };
  kcontacts = {
    version = "5.84.0";
    name = "kcontacts-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcontacts.git";
      rev = "afa4ae7548e6ae7711ec34ba54bb8d213080c318";
      sha256 = "1lpskbnccnga47qkig3rw8d5av9b1s4swp2akbv3bdhpxy2zppwi";
    };
  };
  kcoreaddons = {
    version = "5.84.0";
    name = "kcoreaddons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcoreaddons.git";
      rev = "c1eea3223b6448de80b99bbe297475c7c3383576";
      sha256 = "0n40gliqcbxs74jjjjklzrcm55ps4zhjkg0fprnrg8ysrgpk3six";
    };
  };
  kcrash = {
    version = "5.84.0";
    name = "kcrash-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kcrash.git";
      rev = "701cd368863bcda384850bbb6b1ab996a1e8fe46";
      sha256 = "0nf229nkcy9zq1az5w62l17hbs2b1zr9accdfvp7a2vf5z5zx8af";
    };
  };
  kdav = {
    version = "5.84.0";
    name = "kdav-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdav.git";
      rev = "d47ce6757889b3a2be0fba994237422de0b715b5";
      sha256 = "1lzcp43l8cpijc5sn2mjvh93na2chry6dqgxi32y87wsmhqri1m6";
    };
  };
  kdbusaddons = {
    version = "5.84.0";
    name = "kdbusaddons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdbusaddons.git";
      rev = "e8213265d3db49eb34e3989371dbcbfa5d8f1b6e";
      sha256 = "0vnzl7avdhrkbn81dfq33kycgmh0vavh1b4d2dmr1hip0xnn90pn";
    };
  };
  kdeclarative = {
    version = "5.84.0";
    name = "kdeclarative-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdeclarative.git";
      rev = "9f0724737e541d55f9b35be83f97a5652947c161";
      sha256 = "1xp9g48w9cwzxw37akm2hz6ajph70mnl669zmywm4n1mrm6rgds5";
    };
  };
  kded = {
    version = "5.84.0";
    name = "kded-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kded.git";
      rev = "5c3ef36872b25ebca1ad8aa22ad9cfc5ad28006f";
      sha256 = "0xxyg3l40yg780grj8ipgwv9sb46ndfpaidv0f8afasl67l7rkwk";
    };
  };
  kdelibs4support = {
    version = "5.84.0";
    name = "kdelibs4support-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdelibs4support.git";
      rev = "bd5c6518c468b6de67ac052811902e70300c132c";
      sha256 = "0gim2wxbarzg02fjwdmq5rpxgjmyrsb3zf4p6bhvk9w8hza60yrv";
    };
  };
  kdesignerplugin = {
    version = "5.84.0";
    name = "kdesignerplugin-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdesignerplugin.git";
      rev = "8141fca6a3939eb8faf461f30b59af554d7be326";
      sha256 = "15wq2npcjjr84wyysq9k2cnxm7ydkmhvsd3237h2p22msi6f0214";
    };
  };
  kdesu = {
    version = "5.84.0";
    name = "kdesu-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdesu.git";
      rev = "45a82b6ad833fb4cafd29584019355c6bfd3774a";
      sha256 = "1ilc4zmjzjxdqwx7w8wwv8sjdsw24m494j46swm4ixh10q8yhi6d";
    };
  };
  kdewebkit = {
    version = "5.84.0";
    name = "kdewebkit-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdewebkit.git";
      rev = "9e2091456aeb3851a3125a8cd97fa744a91e7407";
      sha256 = "12gbz8vkknwxxz32hsm9v6amwq9217jl2x8kg62c4g1yccbi1h35";
    };
  };
  kdnssd = {
    version = "5.84.0";
    name = "kdnssd-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdnssd.git";
      rev = "5a5e100353aefcf3552342c6bbb22d45d45266d0";
      sha256 = "0w41ai5bvz3hw0zk7423fs2y56574q19g1r42hqm3bs4kqgxdcd4";
    };
  };
  kdoctools = {
    version = "5.84.0";
    name = "kdoctools-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kdoctools.git";
      rev = "1a544d99a5d22009c1dff3989e3916517d75a793";
      sha256 = "10yydz25lyd81qqsslqsrb4b33q1xw45plbfrkwy30h9rszm68mw";
    };
  };
  kemoticons = {
    version = "5.84.0";
    name = "kemoticons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kemoticons.git";
      rev = "d8d513310b7902b9097244dc50083ba78c3ca474";
      sha256 = "0xgi6y0hqvmdaj1nbhhs8823c552by29i7w4hrbd19s3ic9chx15";
    };
  };
  kfilemetadata = {
    version = "5.84.0";
    name = "kfilemetadata-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kfilemetadata.git";
      rev = "df7a56760485c0f6f5bc08cf66d3c2770dc34859";
      sha256 = "0gab65mwl3xg7xfxnaxshdb2xpg6x72v58wi0sp2cbyy9898f6dp";
    };
  };
  kglobalaccel = {
    version = "5.84.0";
    name = "kglobalaccel-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kglobalaccel.git";
      rev = "ac6c8dffa580772ed99c20cb0c42b4072a1ceae3";
      sha256 = "1yxnw9fns3iyf3lq8ggfgs76mcr7jpb8rigccpmh707cjrwji2vv";
    };
  };
  kguiaddons = {
    version = "5.84.0";
    name = "kguiaddons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kguiaddons.git";
      rev = "45b830aa7e00512b013b660dfbad6de18f9bf08e";
      sha256 = "1awi5g1q7j9v52w3dxxx3i9yzk3dcqziyp4z2jai6c7rqq6xwpvq";
    };
  };
  kholidays = {
    version = "5.84.0";
    name = "kholidays-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kholidays.git";
      rev = "a00d0fe67c98d9c5921df2679bc3e096295a884a";
      sha256 = "10vlri7vb16qq2f3zi4ichgkfz1p63dafwpvhky4dfparvhas4q7";
    };
  };
  khtml = {
    version = "5.84.0";
    name = "khtml-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/khtml.git";
      rev = "a34c800203c96b2d52c374a13306c9a9722ced30";
      sha256 = "0nhz5qlrnsy1pzy52b9kznnm2si0dn010fgvwigv3x7j9jhsg9s5";
    };
  };
  ki18n = {
    version = "5.84.0";
    name = "ki18n-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/ki18n.git";
      rev = "f3e663b56ae6779577453a90456754f3a2f62cc6";
      sha256 = "1cn7x3a94rh5cyy11idzd6nkppp5wjgplsllpg0lb6w8wbm69bpm";
    };
  };
  kiconthemes = {
    version = "5.84.0";
    name = "kiconthemes-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kiconthemes.git";
      rev = "52064e0fe0fb0d0f6e01b7f915b52c06b54addf6";
      sha256 = "1ayk1lp9sh0lmxzfq4pk3vhwf33s6j0wakgd9g17g26ynzqxaviw";
    };
  };
  kidletime = {
    version = "5.84.0";
    name = "kidletime-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kidletime.git";
      rev = "ec6e0b63ade39d6af121e84411648ad0173e5561";
      sha256 = "0pns74c3lql5dkfk67spqxdy47yv18aa4jq03riwda57iar6n8h8";
    };
  };
  kimageformats = {
    version = "5.84.0";
    name = "kimageformats-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kimageformats.git";
      rev = "7f56d835f050e5b15b82f54e1582a43c71700a5a";
      sha256 = "0fxhzsd5ll2bby7klbnkch05mz59rngyk6f9sn8x82qdkprgp8c7";
    };
  };
  kinit = {
    version = "5.84.0";
    name = "kinit-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kinit.git";
      rev = "e6668c437ffc2c489ff70f05adc5cfc8ced602fc";
      sha256 = "0k1vkq8bnrdiwms6a9gx09074hrljiigxbn883qdk7x11pcp04jy";
    };
  };
  kio = {
    version = "5.84.0";
    name = "kio-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kio.git";
      rev = "f0664a0cbd9bf70326a02bac4ab8f4fac8b152ab";
      sha256 = "0dic8r67bjx3199gm03l9wmw4lp249s9nirbrizgckqwrb86lgxw";
    };
  };
  kirigami2 = {
    version = "5.84.0";
    name = "kirigami2-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kirigami.git";
      rev = "040e6136904ad4f4b0e7f1a25a872fdb6575db5c";
      sha256 = "13jfrafv684clbjxzsy7ssdg1nxjd2yf6iq3l29yf9ppd9j273ya";
    };
  };
  kitemmodels = {
    version = "5.84.0";
    name = "kitemmodels-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kitemmodels.git";
      rev = "b703c6ff578b000a52b22a1d3a21f8eb27c0c625";
      sha256 = "1kcsrfna9z9li19g3wndkzxh0agx6kv82q62ablmraddxb6apjan";
    };
  };
  kitemviews = {
    version = "5.84.0";
    name = "kitemviews-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kitemviews.git";
      rev = "f2786d04dbb14c9d00e535f155e28c9e35aa4271";
      sha256 = "0h0hphch30qgk7vpsy4a620pgvpa2pfjfkz4yzsp7205rz6hp763";
    };
  };
  kjobwidgets = {
    version = "5.84.0";
    name = "kjobwidgets-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kjobwidgets.git";
      rev = "45f58fdb4c205fa77b643207d6db68f7021ccf6c";
      sha256 = "0sw1bd0gkfywd5yihzr4ma59mappl5wjz648lpm0xzp55qv22hjl";
    };
  };
  kjs = {
    version = "5.84.0";
    name = "kjs-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kjs.git";
      rev = "1157752e07b10302f79a90d19f733ad88b45cb91";
      sha256 = "1cra6hacvih9k7zhp8hy1lqq0b8c899ghqylwghax98mqfv2zaik";
    };
  };
  kjsembed = {
    version = "5.84.0";
    name = "kjsembed-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kjsembed.git";
      rev = "1e8b8257e38f31c8f3b30ec0ed4534f91586f588";
      sha256 = "0ryk6zdnvj03ljyhgyhzvlqsc38z89bxlzkmvajwpdps4m6427dz";
    };
  };
  kmediaplayer = {
    version = "5.84.0";
    name = "kmediaplayer-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kmediaplayer.git";
      rev = "8385c6ba9c00a3a0b62b52550db73cfa83d405ae";
      sha256 = "08f4xpcvp4zx5wlnmx7y2mff4kq0wqrps97sqz7vrdif03wmhqhx";
    };
  };
  knewstuff = {
    version = "5.84.0";
    name = "knewstuff-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/knewstuff.git";
      rev = "1b01097c5f9e7cd07e9f3ea284ab3695d9cfafe9";
      sha256 = "01xaia4ig2zjl901gygzzy2kymqbql09d46w0nica0g8kfcz2wp2";
    };
  };
  knotifications = {
    version = "5.84.0";
    name = "knotifications-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/knotifications.git";
      rev = "30093313569859f46619fd9f81ec70b44b60e175";
      sha256 = "0j85yj5l2y7zf9l62q3qv3669kqmjgfdlpgipp36rb5jbjpijxg8";
    };
  };
  knotifyconfig = {
    version = "5.84.0";
    name = "knotifyconfig-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/knotifyconfig.git";
      rev = "bbcae0e42acc9a0c97e20abdcfc44793b116f9cc";
      sha256 = "1bzk87d0v6bbzvmp4kpl6qjpizchxrznlzh0ya74bsngiwhhpz7d";
    };
  };
  kpackage = {
    version = "5.84.0";
    name = "kpackage-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kpackage.git";
      rev = "0e03365fe2674c229ff8c93a353c7c9ce0302afb";
      sha256 = "0crmrr4xc713kpclsban1fz6rz5wzbkvfvnq2vv3vaixd9j6n96a";
    };
  };
  kparts = {
    version = "5.84.0";
    name = "kparts-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kparts.git";
      rev = "8b81bf476c245e4d3c4c6e2edb97db160e2da2bb";
      sha256 = "0f7k888g8gp76xxihldzdcfd9w6dqvlhgxasgx1ggrnf6mk5gcm3";
    };
  };
  kpeople = {
    version = "5.84.0";
    name = "kpeople-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kpeople.git";
      rev = "3f4ab345ea3dd08f06900a5583f6da73a2552a62";
      sha256 = "086sadachiiwph5iah4v4k27hm6f9js4ip2078i03lqdm1v6vkji";
    };
  };
  kplotting = {
    version = "5.84.0";
    name = "kplotting-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kplotting.git";
      rev = "96be9c3cc47a6ce82bd626d2c8f6aa1a2622acaa";
      sha256 = "0g2r3z42k8j0l8rbpv5kbg4y7gcmriizpc6g42a1qaxbj5nksp9s";
    };
  };
  kpty = {
    version = "5.84.0";
    name = "kpty-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kpty.git";
      rev = "09b4e065624ee9e00d2743bbeb878e1ced988728";
      sha256 = "02bwb3dzgbqp0gqypiajn739lw3kl37bj56c6wl4pprfgb2wvfrk";
    };
  };
  kquickcharts = {
    version = "5.84.0";
    name = "kquickcharts-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kquickcharts.git";
      rev = "09e964200c903d31295bbf04a018a4123a04f496";
      sha256 = "1kyij2jss3lv91l2qn2p4b4f4rpwq9ndav175yg8wqdd03x9485x";
    };
  };
  kross = {
    version = "5.84.0";
    name = "kross-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kross.git";
      rev = "2e0b5b7c1fd8094eec5fd93fa68625b675f0cf93";
      sha256 = "1ax8b1qb4i4dslyd7dmgsl7ya6an491bwqbr41zjwm2n86qdzlvh";
    };
  };
  krunner = {
    version = "5.84.0";
    name = "krunner-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/krunner.git";
      rev = "e067c4f4cd2f428a512526d2c157a7bc5d7d640a";
      sha256 = "16xrxg1r4apnqgqwjqzjyxbid369w98qpnqx0fb27z580jm9ll3x";
    };
  };
  kservice = {
    version = "5.84.0";
    name = "kservice-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kservice.git";
      rev = "0b569b01fb7fafbade8ce6ed090fba6af9705262";
      sha256 = "00nb7m7p5ag7f2xpks8b93kvmz4r6dc8s5pyr86fk7dsk0q55w5g";
    };
  };
  ktexteditor = {
    version = "5.84.0";
    name = "ktexteditor-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/ktexteditor.git";
      rev = "96182d6347c40bf80d40883da42f2ed2ce61e902";
      sha256 = "009w9drqbq303y0l0yrfci772vv5nmkizpchb16z1gdy9b7j1i07";
    };
  };
  ktextwidgets = {
    version = "5.84.0";
    name = "ktextwidgets-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/ktextwidgets.git";
      rev = "042f5a7f79a008093f9af7cca9a7c3fefd21e7d4";
      sha256 = "0r774ljnfq69h8dhf4ipr45s9iqa8h5j58jvqcnbdzn9d1l3f59p";
    };
  };
  kunitconversion = {
    version = "5.84.0";
    name = "kunitconversion-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kunitconversion.git";
      rev = "eed860dfb630e1e46d7f4d377ba97d6873714e1f";
      sha256 = "096kms1jniyzlvp5vy3s4s9swjdcvx7mgb4hdbw3a4fmkp9prjik";
    };
  };
  kwallet = {
    version = "5.84.0";
    name = "kwallet-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kwallet.git";
      rev = "60c8e9d9537c30efe44626651d683f847e38a5f7";
      sha256 = "1sgc9v8d3df2m10ibc6l73xz3i7p9ng1prkvih2fb79cx2ix2fs4";
    };
  };
  kwayland = {
    version = "5.84.0";
    name = "kwayland-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kwayland.git";
      rev = "05ded8ff28d534d6775a4a734d77848df33f5258";
      sha256 = "1iwjx70hdzf1bznpvqwnb9f3gd1wc5mbh74wpigwzjgsyqrn91cs";
    };
  };
  kwidgetsaddons = {
    version = "5.84.0";
    name = "kwidgetsaddons-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kwidgetsaddons.git";
      rev = "6a72f111ab1140a0144ebdcd6c7b4af009d0093f";
      sha256 = "1cfwb5201gx54bnrg2fcls1dflaz2k3zgmpsj8llnag856p1rvq4";
    };
  };
  kwindowsystem = {
    version = "5.84.0";
    name = "kwindowsystem-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kwindowsystem.git";
      rev = "26e295a3c0ab0b37105c09220fe8f01b8d831203";
      sha256 = "0b64zrbb8v7ych9b4f85gb11c3s1wn7pavxiyjvi2mzxqz6z7741";
    };
  };
  kxmlgui = {
    version = "5.84.0";
    name = "kxmlgui-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kxmlgui.git";
      rev = "917b743de34c06fde63779b874e646f6dd6a90b3";
      sha256 = "18y78w21qrxw7637hcwbvk1w3ml7miakml7365d8lh51wfxs6sw3";
    };
  };
  kxmlrpcclient = {
    version = "5.84.0";
    name = "kxmlrpcclient-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/kxmlrpcclient.git";
      rev = "dcaf14c9ece61525806ce2025615bdcccc870177";
      sha256 = "143qhc8749jmmma6kj8s246dlgdwx1x0gindx0nx2ja5i502z1gq";
    };
  };
  modemmanager-qt = {
    version = "5.84.0";
    name = "modemmanager-qt-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/modemmanager-qt.git";
      rev = "d228c823dfde390f24ccf7cf235108b4297941d3";
      sha256 = "17qdk3m857356a62mcsv8vpdaznjwqr1w7fyqv385xci31j8w2qz";
    };
  };
  networkmanager-qt = {
    version = "5.84.0";
    name = "networkmanager-qt-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/networkmanager-qt.git";
      rev = "898e97695c523733a486271604d43c4060ac71ba";
      sha256 = "1b96bz096rv5hfgxql1n866idzxi36adslflw9cfa12cnn33ywhi";
    };
  };
  oxygen-icons5 = {
    version = "5.84.0";
    name = "oxygen-icons5-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/oxygen-icons5.git";
      rev = "3800f6fed0b44ac36a64e3304b1ed3049ceb29c4";
      sha256 = "030iiwh1mq4dzi6c3kw9aa3pjxnnnd6006317lslc2lbxsqpxh42";
    };
  };
  plasma-framework = {
    version = "5.84.0";
    name = "plasma-framework-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/plasma-framework.git";
      rev = "2ec68a6148506e4fcbd165ee68956bc055e4e96f";
      sha256 = "1x9dwrsv5vvya7zvlnw58gln0vy2hcvp2dx9m0nhjixazmw5ilpv";
    };
  };
  prison = {
    version = "5.84.0";
    name = "prison-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/prison.git";
      rev = "4fee056beb8fa46f29df0352d0eca9f49f0be7a0";
      sha256 = "04xpcfdwi9lgy2wma7v6rq9vviviwnzxr438zw2z4plkqak3isx6";
    };
  };
  purpose = {
    version = "5.84.0";
    name = "purpose-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/purpose.git";
      rev = "313092d8e772828d000b038a5d09083688e5282b";
      sha256 = "02i1hh0y65czk4347fakc9743xlxdhaaiq56gvlbhrxq97q580k1";
    };
  };
  qqc2-desktop-style = {
    version = "5.84.0";
    name = "qqc2-desktop-style-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/qqc2-desktop-style.git";
      rev = "4db9bbbae2c2ed83786256da4eceba536e8f210e";
      sha256 = "06p95nfipkrr58dlk3splpi93bbbvwypm6xaxfy2hvz039d0z1f1";
    };
  };
  solid = {
    version = "5.84.0";
    name = "solid-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/solid.git";
      rev = "9829c51a04ec11621023932f9634a135f8e9dd8f";
      sha256 = "0qwy2i6asqhxak023x500fm87i64zjymck46q5xhxmgigzklfmmn";
    };
  };
  sonnet = {
    version = "5.84.0";
    name = "sonnet-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/sonnet.git";
      rev = "7f36a22e81286e13ef69c9e2d4d6a36f231b04c8";
      sha256 = "0dyxppnld5jrn5fi4qa114054f016m49i37i87cw4nrmpcpm3958";
    };
  };
  syndication = {
    version = "5.84.0";
    name = "syndication-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/syndication.git";
      rev = "43f387604cd16669e1dc7ce546b703cdb642d87b";
      sha256 = "1spc1qiarmbg4957sbqy4j1dg6f9zblg1516f7fp53ygcgz8fg03";
    };
  };
  syntax-highlighting = {
    version = "5.84.0";
    name = "syntax-highlighting-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/syntax-highlighting.git";
      rev = "a2bb6f45cc29d3122fa1825a15aa210ef4c2485c";
      sha256 = "1r20m3kc1wxxkkknmps3znbfwmjxwjghb6sjn4x4iqdfk9yp8qmw";
    };
  };
  threadweaver = {
    version = "5.84.0";
    name = "threadweaver-5.84.0";
    src = fetchgit {
      url = "https://invent.kde.org/frameworks/threadweaver.git";
      rev = "b7953f0e9bb2824c9763c6d26375d56d34c95190";
      sha256 = "17jlk5kbhbd5c6r759bk40ffm2if63rn03fxvmf0qmry1fh4p6m6";
    };
  };
}