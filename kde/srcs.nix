{ fetchgit }:
{
  akonadi = {
    version = "21.04.3";
    name = "akonadi-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi.git";
      rev = "78ccf477a1cf6de764e8c7fd4c3fad490de76d58";
      sha256 = "1ljqbhf50grrxys53jlj7rmhpmylpw5mj094g6v33409l9sdrrmd";
    };
  };
  akonadi-calendar = {
    version = "21.04.3";
    name = "akonadi-calendar-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-calendar.git";
      rev = "baae0215a7d7558c10a45eecdc44239ca19693ce";
      sha256 = "0dyp4k71ndhmfj816c3vg2avifkl2x1ppv6iwy3bdsbcajn4f479";
    };
  };
  akonadi-contacts = {
    version = "21.04.3";
    name = "akonadi-contacts-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-contacts.git";
      rev = "7dd573ce7307cdd9997cd452ab9101d35dd2713e";
      sha256 = "0dhplpy0dazz3k621is4qq8574g9z72l7bjhi23ka1lzz30c183w";
    };
  };
  akonadi-import-wizard = {
    version = "21.04.3";
    name = "akonadi-import-wizard-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-import-wizard.git";
      rev = "f4ac9facec0d5a3967fd02eb0fcb511e7872c3b1";
      sha256 = "1ahsmwa694fqnczzsx4agb21bfjnpa7vql48c976yjv3a3gz1p1d";
    };
  };
  akonadi-mime = {
    version = "21.04.3";
    name = "akonadi-mime-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-mime.git";
      rev = "5012eb8c7d34c662e5e12a135449240807377c79";
      sha256 = "0f2b14a8v18cwc0jwbcixj4nwz478vg8gxyn61swv2a993ammn01";
    };
  };
  akonadi-notes = {
    version = "21.04.3";
    name = "akonadi-notes-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-notes.git";
      rev = "4dc557546ff3f2d156dfa66ff7ab2356b1eba4a0";
      sha256 = "13649azg6d2wpb90n0fb7a8sfs8v78pdnrybxah97a7mgw5x5srd";
    };
  };
  akonadi-search = {
    version = "21.04.3";
    name = "akonadi-search-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadi-search.git";
      rev = "26c21d0597bb44a9a1b6677c68b81e1377af9bea";
      sha256 = "0pf8xb4915magpprww8kji2mglkm91a3j7lfv0flb1c0f6f201bj";
    };
  };
  akonadiconsole = {
    version = "21.04.3";
    name = "akonadiconsole-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akonadiconsole.git";
      rev = "7eef7f3a53f39ab598b007c8c56ac7663e2f3810";
      sha256 = "0p7q5ym9qbvgjd4chf3gvk6xkipb8zdf0p1haqlh5s0nbhhnpbql";
    };
  };
  akregator = {
    version = "21.04.3";
    name = "akregator-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/akregator.git";
      rev = "f56fb99082c7918337751b8d25c57c63fb5dd47c";
      sha256 = "0ldn8g9sw7a12vd78pysmjr175isicfqs6d2drjp6g2yjg6j2m3h";
    };
  };
  ark = {
    version = "21.04.3";
    name = "ark-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/ark.git";
      rev = "78dc03ab651f5a62321c87b4e2b0a9efb0d6d5a9";
      sha256 = "1i8pra6fkghhiirz0hcyiqi7n3im1z4hprjgcqp8k70nqyl3pxbx";
    };
  };
  baloo-widgets = {
    version = "21.04.3";
    name = "baloo-widgets-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/libraries/baloo-widgets.git";
      rev = "5a3b20acc7e516771190196f848b3c278ac6256f";
      sha256 = "0lwavs40x7nkdqq2q4hpap70lcwls5mx5ifahz2i2gn0wkk1bvpa";
    };
  };
  bomber = {
    version = "21.04.3";
    name = "bomber-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/bomber.git";
      rev = "0902c557fbbadf40ee544154ba491d0ca7cee6a8";
      sha256 = "1v7ndip5907mq5klnbm8y0pff6rlhi0rdk6y07234w9rr1pcybk4";
    };
  };
  bovo = {
    version = "21.04.3";
    name = "bovo-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/bovo.git";
      rev = "856c46e4b0569bbc6f2b7f7a7056333a6d57b705";
      sha256 = "0kqjc93001pxywxl46zrzahxpsvdifvsvzxv54gps5hq469mqf5y";
    };
  };
  calendarsupport = {
    version = "21.04.3";
    name = "calendarsupport-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/calendarsupport.git";
      rev = "a773765b508cc5b8ed9bdb7a184b8f05bdba0e92";
      sha256 = "0in14rc0zh67bnrywnzh401hqlsgj5drbl3ybmjacpyv0v9v4y6r";
    };
  };
  dolphin = {
    version = "21.04.3";
    name = "dolphin-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/system/dolphin.git";
      rev = "2c7a1f4b4c5a7cfd8cd4ddafbe7ca46f9b968830";
      sha256 = "1ar5hlvc24pcwlijmncjvn4q8jblicnx6csgd6b5qynmcac5j55r";
    };
  };
  dolphin-plugins = {
    version = "21.04.3";
    name = "dolphin-plugins-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/sdk/dolphin-plugins.git";
      rev = "4f9910b9a1d38d85a1cd06e784dabefc6e9916c9";
      sha256 = "0lryrshyxx9aknnqwm1rjkr8b7slqslk8db3g03zdvcd5y1ca6sk";
    };
  };
  dragon = {
    version = "21.04.3";
    name = "dragon-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/dragon.git";
      rev = "3a2d074e9382e29692467d2f1bc7c7f9a299b26f";
      sha256 = "1zr3qjhii46am6nlpcgb2km24465garn60hc25xhvfdapwnr5g7m";
    };
  };
  elisa = {
    version = "21.04.3";
    name = "elisa-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/elisa.git";
      rev = "7fb3d07068011933b09437d88e838e4dc4468ee1";
      sha256 = "01mriykbwhm6gah4nm075kfvpc9mw91vixl7p3nfk8qcncm262ph";
    };
  };
  eventviews = {
    version = "21.04.3";
    name = "eventviews-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/eventviews.git";
      rev = "381f3ec6885b7d2b32f53e3cf96a6a81ddbc6998";
      sha256 = "1nrby5av1gy3i0yaamasdl9s7ixgb11l83fpcyqsj3s7yj005bi2";
    };
  };
  ffmpegthumbs = {
    version = "21.04.3";
    name = "ffmpegthumbs-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/ffmpegthumbs.git";
      rev = "76a13b3cce3f4a362e1f9a3a3077abb168efa593";
      sha256 = "0vnp68ngw9bsx4gn0fdk4znbpzjqb98npczyn7n7c5mzahgqlj8h";
    };
  };
  filelight = {
    version = "21.04.3";
    name = "filelight-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/filelight.git";
      rev = "d62b437c66e3e3ea9e192914be659c3a842ffc84";
      sha256 = "05zby0z5mvx11d9c9zjr0naz2z1c2vvfvr6kfciy7qbwwik9nry5";
    };
  };
  granatier = {
    version = "21.04.3";
    name = "granatier-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/granatier.git";
      rev = "e3d3c9a34395f76aaab5ab35d3bea4ae7273f8f6";
      sha256 = "1byjgch2ak5s1sn4wris7g04qaj63lxyhg2lmk4kdfpg83z1yn2l";
    };
  };
  grantleetheme = {
    version = "21.04.3";
    name = "grantleetheme-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/grantleetheme.git";
      rev = "c2d1dd4b9b102a49783deea4486208f5b7e14ecc";
      sha256 = "1lgnbdddi8ab5j4anfvhg3zbq4j4472g86kaz568av3s6x0lrs0x";
    };
  };
  gwenview = {
    version = "21.04.3";
    name = "gwenview-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/gwenview.git";
      rev = "5681c50481c303d60f15a87829d5e25069dec51e";
      sha256 = "0v19wffhsrrd2hwkl8d5wyrhpnzsknp2bvx5avz6a8p8g5ssbkp1";
    };
  };
  incidenceeditor = {
    version = "21.04.3";
    name = "incidenceeditor-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/incidenceeditor.git";
      rev = "f49fe331bc6d01e17221f2a6d77255e06292a14d";
      sha256 = "0fakj9y7ydc22wl6s3n3ccnd1a2i96pwx0qhf1xmf752fk6l6mgl";
    };
  };
  k3b = {
    version = "21.04.3";
    name = "k3b-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/k3b.git";
      rev = "fbea53425262edebbcf5cba4fc8297e98c773509";
      sha256 = "0hm291c60las7a2cfis1fnhgc5k78x7in1f1i21mxg4hrg7z5f2q";
    };
  };
  kaccounts-integration = {
    version = "21.04.3";
    name = "kaccounts-integration-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/kaccounts-integration.git";
      rev = "7660f66662e9e934c087627d45c7e0828ce109f1";
      sha256 = "15ljar2yw7b93awj4pfn824bk1219zh2vjlczvaawky81xpafjpk";
    };
  };
  kaddressbook = {
    version = "21.04.3";
    name = "kaddressbook-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kaddressbook.git";
      rev = "dec732643dcc85958819b94e842f85e46d74550d";
      sha256 = "0b0pg546dkgcjg75bjnpvkmr3r3wv49lfsaj43dc6anpj74sxyzi";
    };
  };
  kalarm = {
    version = "21.04.3";
    name = "kalarm-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kalarm.git";
      rev = "503c8fd5c985d392e0bf2a64f9e3c683d3c1915c";
      sha256 = "129r6jj4m6rqpny9nj4pw08d9x5ynnkzgg6jd4ahsgqzfp81k2cx";
    };
  };
  kalarmcal = {
    version = "21.04.3";
    name = "kalarmcal-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kalarmcal.git";
      rev = "c773ba43d411fea8e81d30cc940d9c94caad23d9";
      sha256 = "01sv0qnsndhj50d1l8ibccdr8wg2fdd3jhwxlyf72fp7bim369b2";
    };
  };
  kalzium = {
    version = "21.04.3";
    name = "kalzium-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/kalzium.git";
      rev = "bad2899167b1abb522ba23f843b617b73f79804b";
      sha256 = "19hijj9351f1xsid2xbcrlrb6h114jqkxifnsrswh2zjryf9brw2";
    };
  };
  kamoso = {
    version = "21.04.3";
    name = "kamoso-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/kamoso.git";
      rev = "fda571bda5eef55d00be93a0a7564d252f13862a";
      sha256 = "14yq23zczg6vfrqj0682ns98kafmfs54bwhynk0jl3zjcr98p4rq";
    };
  };
  kapman = {
    version = "21.04.3";
    name = "kapman-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kapman.git";
      rev = "19d549fe2ddfde483e961ceb254784e0c09e539a";
      sha256 = "11xppcg4c195zhxjpaxa9kpmbvpclkcnh9hlp3vd16i9k2l6abmp";
    };
  };
  kapptemplate = {
    version = "21.04.3";
    name = "kapptemplate-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/sdk/kapptemplate.git";
      rev = "27f61d74c709c8f015d473ee23b90611c2097065";
      sha256 = "0xzwj0g9j16hv93pdvan5ksqx772lhicaa4wkkidj6yqxy8f6hgw";
    };
  };
  kate = {
    version = "21.04.3";
    name = "kate-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kate.git";
      rev = "809548bf0fd9b5ab3d391881f909919cb5753ee6";
      sha256 = "11gpa1vsgj0cpqzmb2ss0zainiiwisk13zb3fddv1qxbz1j4hy05";
    };
  };
  katomic = {
    version = "21.04.3";
    name = "katomic-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/katomic.git";
      rev = "ac8eb01341cc1eecd78f5ea8238b7e7450f021de";
      sha256 = "0a0v6rryph07k48130ibbwr9f0qic7mkwlch5yychkhadh9vm97c";
    };
  };
  kblackbox = {
    version = "21.04.3";
    name = "kblackbox-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kblackbox.git";
      rev = "c63d2e303605fb6a6716ebebcbc73aade6ea84dc";
      sha256 = "1ahxj7l8zfsnf4wm3kwczk3fz7rc8bn30fkw2d5mql2iq2b5zbxx";
    };
  };
  kblocks = {
    version = "21.04.3";
    name = "kblocks-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kblocks.git";
      rev = "59ca52a6a9df95cf638a280b8a16581cc7713dac";
      sha256 = "1j1rbg114myxfpcj0rs2l6cawy7w996maym1m2yglpkdci7rf8cg";
    };
  };
  kbounce = {
    version = "21.04.3";
    name = "kbounce-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kbounce.git";
      rev = "1ff4d8d8b368b5e93e5cb4b66558742c71ee9e68";
      sha256 = "15r5r7a33ksfb3mrhncaimhlraq0fbhwj6iwh416y2pj1g95l15r";
    };
  };
  kbreakout = {
    version = "21.04.3";
    name = "kbreakout-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kbreakout.git";
      rev = "36139787907811c6290b181ba47e2b291b8d4343";
      sha256 = "0bmgrwg0mrm3a32pya6sxmq3aisjyjxvcp49slfy2cplafx9csxz";
    };
  };
  kcachegrind = {
    version = "21.04.3";
    name = "kcachegrind-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/sdk/kcachegrind.git";
      rev = "0e6745f38d05c120034fdcf0e3bbfe0db9b1b213";
      sha256 = "0jq3l42shpslkm9x02fpd14c8wvpzv84wbl1w85kiddmacy29xbc";
    };
  };
  kcalc = {
    version = "21.04.3";
    name = "kcalc-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kcalc.git";
      rev = "22dd01fb9754d074fb65bb929b19d81010af587c";
      sha256 = "170wgh0bj7ghp9fbryz9lnr2iha5ja1y8mjlh05fl2786zbbji4n";
    };
  };
  kcalutils = {
    version = "21.04.3";
    name = "kcalutils-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kcalutils.git";
      rev = "697195969041e66bb526e64b69b641e0cd8afb92";
      sha256 = "13lg0xik6lsshmw19ybpq3j0kjivlp0lw6mc7bv4mj2c7a6d7lsf";
    };
  };
  kcharselect = {
    version = "21.04.3";
    name = "kcharselect-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kcharselect.git";
      rev = "53bc45e6f47dbd9c3f09e102f03d15120585997d";
      sha256 = "0wyg907zgrdh05irnqing6m2wzpzvk20lf4w9kh5b7243r06z4lk";
    };
  };
  kcolorchooser = {
    version = "21.04.3";
    name = "kcolorchooser-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kcolorchooser.git";
      rev = "29e79c134840dbf01db746d4b59b70554c9eb454";
      sha256 = "0q7va1r754n25zam2jw7b7i4qk4pm08asj52rwx7vknajhfng16b";
    };
  };
  kdebugsettings = {
    version = "21.04.3";
    name = "kdebugsettings-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kdebugsettings.git";
      rev = "82ea95eef5c1c77c99b0f70002071020f9c3f8df";
      sha256 = "1aksn7x04280pw9pk42b0qkvih5az2f14q88547hiql1cpaxx7fd";
    };
  };
  kdeconnect-kde = {
    version = "21.04.3";
    name = "kdeconnect-kde-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/kdeconnect-kde.git";
      rev = "ac53f3f3cb4740ebafb11abf91f3d9a4d3156d6c";
      sha256 = "13wvrf12al23qdphcrcvbnai1wxq1cp37kpivycwjsnbv839in6w";
    };
  };
  kdegraphics-mobipocket = {
    version = "21.04.3";
    name = "kdegraphics-mobipocket-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kdegraphics-mobipocket.git";
      rev = "9c61298477a3b4a65ff1ec3511ae833238e87dbe";
      sha256 = "0cg35av7hddlllzmn2r08ck90dwzln5vwcqmmgvlkhskdga6asqp";
    };
  };
  kdegraphics-thumbnailers = {
    version = "21.04.3";
    name = "kdegraphics-thumbnailers-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kdegraphics-thumbnailers.git";
      rev = "3f941f1261ff0a8192c99134813088978a52f47e";
      sha256 = "0sj6bh8hg5x4637jla5k3j7lhz30bli8aajl9wfxp80ab83wimw0";
    };
  };
  kdenetwork-filesharing = {
    version = "21.04.3";
    name = "kdenetwork-filesharing-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/kdenetwork-filesharing.git";
      rev = "0abade988be4c1f29f821c3176b3a650be6f8ca6";
      sha256 = "013klk4kp2yamzjid6ck9lrhm2nmmxz1zq568ca2dglxz6lxpx8s";
    };
  };
  kdenlive = {
    version = "21.04.3";
    name = "kdenlive-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/kdenlive.git";
      rev = "ca393a7fe78407f1e2642a77bcbfc5ce97bf9de1";
      sha256 = "10w9s95g4jd2mb2hpm5wz2ciwhrcc8r0c36jynzl1p6ip3vp6n9v";
    };
  };
  kdepim-addons = {
    version = "21.04.3";
    name = "kdepim-addons-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kdepim-addons.git";
      rev = "329f63a7d832c0b8c3e660baba15914c0b0409f0";
      sha256 = "0wknlflaq19dr5flvd9gydbmk4lkzlb817kgw3xpdh3wqfn7a922";
    };
  };
  kdepim-runtime = {
    version = "21.04.3";
    name = "kdepim-runtime-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kdepim-runtime.git";
      rev = "4b24843e0948d1f2a90e20d2af690a39d7a6f8bc";
      sha256 = "18yf4sn22980r7bhk7810qmvrrq1brb2m3cgmvdmpw177ch3vivz";
    };
  };
  kdf = {
    version = "21.04.3";
    name = "kdf-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kdf.git";
      rev = "9b46f3184ef89ba1047a1063d11edadf352b8f99";
      sha256 = "1fc29dbj8ig1jc6kb7nqpn9r4035c9185qqkj2pgy4jm1n9wzjni";
    };
  };
  kdialog = {
    version = "21.04.3";
    name = "kdialog-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kdialog.git";
      rev = "3c9c5e3a9f573c9569e44889f6c7f8667b1caf99";
      sha256 = "1ws80jsr28v36chvy0fvwvwc39lix92f8azzfvwcfw8d57gckd6k";
    };
  };
  kdiamond = {
    version = "21.04.3";
    name = "kdiamond-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kdiamond.git";
      rev = "087b03012fb7251614ac6a4ec3db062b71fa3c58";
      sha256 = "1qd3fvg5bljrpdqmpsm1f7xaic3ci1914fmn5hlm3pw8mxymjr7s";
    };
  };
  keditbookmarks = {
    version = "21.04.3";
    name = "keditbookmarks-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/keditbookmarks.git";
      rev = "956c0e3dc23efc364a302ac81044b3a67a3a2dc2";
      sha256 = "1lfh5rl11y1j6h5f1cav55fj7q9dl6d0mf73rzqgkw3zkx26qqm2";
    };
  };
  kfind = {
    version = "21.04.3";
    name = "kfind-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kfind.git";
      rev = "1e525b4bd297fa9daa9be873e8874a21f79beb3d";
      sha256 = "1id4na9fvm3vzz5jbfcp94mbra0zkppcsmmg20s22zp5s1w38s46";
    };
  };
  kfloppy = {
    version = "21.04.3";
    name = "kfloppy-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kfloppy.git";
      rev = "77af6bedb2e9b0c63fa810f748f3d8e8f61666f5";
      sha256 = "1g6d1x5w7kqgwv143mahkbby2028jhri8yag8kpgfgz8a2224bz8";
    };
  };
  kgeography = {
    version = "21.04.3";
    name = "kgeography-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/kgeography.git";
      rev = "c64e87c84703ee2ebccab32ba7280c0d9489235b";
      sha256 = "11ci83zy44rjj0rl0pwrpmz35jhzca5rh668yrbcvsh0fvnxbcx1";
    };
  };
  kget = {
    version = "21.04.3";
    name = "kget-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/kget.git";
      rev = "003064ad4b2d6a249ff3f38243d10106447d226c";
      sha256 = "0qyqdzb3zazp6cx46dmns1sn8p182h494myx8yzqj8ls156x0xb3";
    };
  };
  kgpg = {
    version = "21.04.3";
    name = "kgpg-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kgpg.git";
      rev = "374d7a3ba0e811dbb96c6583a34098043639a57e";
      sha256 = "0fw5cc7wykkd96p18h1m7wdfhxk0n6sw7xnlz26xkmsq19cnjhy3";
    };
  };
  khelpcenter = {
    version = "21.04.3";
    name = "khelpcenter-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/system/khelpcenter.git";
      rev = "c82415715dba60a67058d21a1b34f32a5af4ddcb";
      sha256 = "14az9rggl9klanshzxxmng4vcxrng9j24ra2d1ckh1im3ccr31s3";
    };
  };
  kidentitymanagement = {
    version = "21.04.3";
    name = "kidentitymanagement-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kidentitymanagement.git";
      rev = "5a86412fc5a60bf746c98cc15eb9d2ae47bcdf72";
      sha256 = "1h3wswgh1k37dbslyxcnc950ljj3canr1yr9rgwfbsz68mjjq8ch";
    };
  };
  kig = {
    version = "21.04.3";
    name = "kig-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/kig.git";
      rev = "96186b6a7dd1260171c54c54091f42db01407da2";
      sha256 = "1m7wv9zdpnvyghc4z7xvxjnf9bl07nj860sjw5293l19d38jq8zd";
    };
  };
  kigo = {
    version = "21.04.3";
    name = "kigo-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kigo.git";
      rev = "08ebecf60933814827f57201c91c89716813e932";
      sha256 = "0wh15gjd24m3rp8ray3fbb4mcw9z6d0r9gmd4dihasy9yn4f5sls";
    };
  };
  killbots = {
    version = "21.04.3";
    name = "killbots-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/killbots.git";
      rev = "41654e341fdd003e843bd6bf6e8636a8f68251db";
      sha256 = "0wa1c4bkwxsc8gyc03f6kpg6i02i2g14wmfwghp9gjcv62hynyrq";
    };
  };
  kimap = {
    version = "21.04.3";
    name = "kimap-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kimap.git";
      rev = "e0538b4bb78a80a7262a65dd24a375f20ee74dab";
      sha256 = "0k7kldx11cj7qj722pqgvvsz4h5yh4hzfminyzf5lkyb9phg4zim";
    };
  };
  kio-extras = {
    version = "21.04.3";
    name = "kio-extras-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/kio-extras.git";
      rev = "2f2f625394871b8b366c8146c883fe12d2de309d";
      sha256 = "168yyszr25scmdz32x7lns7rvy6wql0sih9pimgq4x1vp96dhndd";
    };
  };
  kipi-plugins = {
    version = "21.04.3";
    name = "kipi-plugins-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kipi-plugins.git";
      rev = "955fc31c6f60c50403e3d5a4bee3f21dee4ce93f";
      sha256 = "1dd6dy7pka7p428lw2cy46jw8m4hkvx5x4zgr6wg9mls18i95w82";
    };
  };
  kitinerary = {
    version = "21.04.3";
    name = "kitinerary-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kitinerary.git";
      rev = "b1464cd06afe549e71320e985f7178daf7ae8a29";
      sha256 = "1kf1i2xq93iafhmrd9iw3vsy37098r25g6g7na6fqzcw1am072za";
    };
  };
  kldap = {
    version = "21.04.3";
    name = "kldap-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kldap.git";
      rev = "4ae869e2af818b1a491d43254f406de1260f8358";
      sha256 = "0z11xv8h03p0jx36qnld43xzpb5v9f6halxh2mm94m83j0cp5dlh";
    };
  };
  kleopatra = {
    version = "21.04.3";
    name = "kleopatra-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kleopatra.git";
      rev = "bb92dfd266e0fc987251e80710440d417f38e07a";
      sha256 = "18crczxrcz8czikachn752pch05gv965k23px32y4qcfx11b51il";
    };
  };
  klettres = {
    version = "21.04.3";
    name = "klettres-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/klettres.git";
      rev = "6f0aff55d8f7adaeef43299f50f27a1c5af582db";
      sha256 = "0zbllfqqsi6h0snc6kydyb9i7v693qr47p0wah3bsidl2763m9gd";
    };
  };
  klines = {
    version = "21.04.3";
    name = "klines-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/klines.git";
      rev = "a981203b013ba5972c7376c01b5d421c07b0221c";
      sha256 = "1nw9qfmq1411881d18wqim4xa0jgg1hvb0bmjf6by9zc88czpik7";
    };
  };
  kmag = {
    version = "21.04.3";
    name = "kmag-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/accessibility/kmag.git";
      rev = "db43b341c1e79c2d0cdf56240d4fbae0873d97a5";
      sha256 = "1x2i5x2gqr3k7dh1a6x7ichjc72p5fc3rgss2qq1z8ka3djvjl4l";
    };
  };
  kmahjongg = {
    version = "21.04.3";
    name = "kmahjongg-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kmahjongg.git";
      rev = "e50f3b1e48827951f78f9fdc0c32cfa73545c91b";
      sha256 = "0svgkr6vzqjmkg9ifq7akpk8anflpm5n72sjiigfmq9lqy3xz82r";
    };
  };
  kmail = {
    version = "21.04.3";
    name = "kmail-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kmail.git";
      rev = "bb906806c4648a645f7b51f6597f6fccfa7b17d3";
      sha256 = "0ywbq0in7fzck3xlbz32vfra9r0bila8v8p7q6y1x2mlrn7ya322";
    };
  };
  kmail-account-wizard = {
    version = "21.04.3";
    name = "kmail-account-wizard-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kmail-account-wizard.git";
      rev = "d0b9f1ae4d817821ae94d1eb345ab6de0a8261fd";
      sha256 = "1ar2ghzfwd17pmz2c1zv3dq8s68fpi0h0xbq1d1rzvnv2wa9n723";
    };
  };
  kmailtransport = {
    version = "21.04.3";
    name = "kmailtransport-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kmailtransport.git";
      rev = "ae8911ba425545b1b3ba71b0f9a7456dcf6aae72";
      sha256 = "1gixr3hcch0bg2m0mscb7igvd27q741xw52581hi7qkhy0rbi7ic";
    };
  };
  kmbox = {
    version = "21.04.3";
    name = "kmbox-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kmbox.git";
      rev = "effe162ba4a63593825b16a085ed7cb2a924120d";
      sha256 = "06fq2cxcaksikcrxnpz34hvxi1v8pa840l66cyypwgksz1ly6wz3";
    };
  };
  kmime = {
    version = "21.04.3";
    name = "kmime-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kmime.git";
      rev = "ebb0fc1da24c8c16e61bc6788e31eae7e2cb274b";
      sha256 = "0kpr8kkac16rswlp9i8jxixm7qvlhybfn2ir4ajryd33b21mna51";
    };
  };
  kmines = {
    version = "21.04.3";
    name = "kmines-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kmines.git";
      rev = "58f37a86b8f9a971d5ce41558a38aa486f42a43e";
      sha256 = "02gd01jfjrl7csl88w217bacavjhdw43j97w2xazixr9p9z5cfvg";
    };
  };
  kmix = {
    version = "21.04.3";
    name = "kmix-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/kmix.git";
      rev = "dd7e4de88c7de0d911837db60ede2177a5ec3050";
      sha256 = "1fai8w0vn9yswngm0gsypp6xdp2sf8fw2kgyykqxriiqw19259nv";
    };
  };
  kmplot = {
    version = "21.04.3";
    name = "kmplot-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/kmplot.git";
      rev = "6cdacb6c956b6fa8e33e2904a99029b42d96bf51";
      sha256 = "1kh1ah70hk7ci1cdkhx91av4675aili9rkkb91grpjj34pffr91f";
    };
  };
  knavalbattle = {
    version = "21.04.3";
    name = "knavalbattle-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/knavalbattle.git";
      rev = "dc5a678b304c404579abc36de8d84b8d51cbb8fd";
      sha256 = "1cs7dcb84a69lrpakbzzhdah9xr8x061dip4r7xrwsvd79w9msr7";
    };
  };
  knetwalk = {
    version = "21.04.3";
    name = "knetwalk-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/knetwalk.git";
      rev = "71b9db789da8dc93ff931c9a4681601ce0263cd3";
      sha256 = "11wb89qli4g3115xqm5s3qid3w4n0a35rn1jpbhpqw57k0k5v7n3";
    };
  };
  knights = {
    version = "21.04.3";
    name = "knights-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/knights.git";
      rev = "593573bd3e0e503a63690529e33cd702b810e0cb";
      sha256 = "0ir3i6jba8qn6v4cimq2qpl9rgvywsg8hcsz52vbw4r2gpkw5dlj";
    };
  };
  knotes = {
    version = "21.04.3";
    name = "knotes-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/knotes.git";
      rev = "1891433d536ce082fc45633f32de54bcbdd5599d";
      sha256 = "187dyq060ymg2r943a3mbw6ilc9cr0yd4iavcs3p2d1p8b741924";
    };
  };
  kolf = {
    version = "21.04.3";
    name = "kolf-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kolf.git";
      rev = "91ecf4f5f35ed95b92de8ffd2c445f3106bc27b6";
      sha256 = "09x3fymbx5w9gj5sv1k919m77d80w5kb45cjppk9r5li06qi74sh";
    };
  };
  kollision = {
    version = "21.04.3";
    name = "kollision-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kollision.git";
      rev = "3bfd59ad8b12a46d6693b9c21bb17b1d29333ec7";
      sha256 = "1h44v2rjl7671k6lbhwgym76ln4di3mvx8fmv67fnww64gqgwjyr";
    };
  };
  kolourpaint = {
    version = "21.04.3";
    name = "kolourpaint-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kolourpaint.git";
      rev = "5849a5f2704a1cc043ca6c96e8a515dc43b27eda";
      sha256 = "13l2jjymcpp3d11qs3pwhcnbv9mwx168bwpzdh2scrlybghpjmvm";
    };
  };
  kompare = {
    version = "21.04.3";
    name = "kompare-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/sdk/kompare.git";
      rev = "6f0305bb3afd7ce28ea7135612d45000c81a4782";
      sha256 = "092zyrmc25391bpjs8as0fbssglg6mskxmx5m64m247w5jplk4vw";
    };
  };
  konqueror = {
    version = "21.04.3";
    name = "konqueror-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/konqueror.git";
      rev = "d825e73d9092e8be262a81356a4337aff3316a9a";
      sha256 = "1dnd2xbpljc9nxmnha409bvpp88a7s9s7y0rn44ykviq477fc48j";
    };
  };
  konquest = {
    version = "21.04.3";
    name = "konquest-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/konquest.git";
      rev = "7309631ba66df7a5599df496bc78f0bb136fa375";
      sha256 = "062wn7vbq3yl3hxw3qyi1qybdsy61r7z1frq2vj78wy3bb6r7pgx";
    };
  };
  konsole = {
    version = "21.04.3";
    name = "konsole-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/konsole.git";
      rev = "dc92c59311856f092f0610be931ee9cd5d74b9a2";
      sha256 = "1bjhjr4hy4bihz6pg4dh1pgrlczgyzqhn3gdmjbn92nl7l56f0f1";
    };
  };
  kontact = {
    version = "21.04.3";
    name = "kontact-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kontact.git";
      rev = "ed3f1b157ae3cb9b711cd39cebbb77f6477d9c24";
      sha256 = "0f5krph9nm5h4f1qy1s3hi8454zlcs5chbw6b5mx91mibrgfzhf5";
    };
  };
  kontactinterface = {
    version = "21.04.3";
    name = "kontactinterface-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kontactinterface.git";
      rev = "7bec4053f7c8d8226981d0ba2a318725603f6fbf";
      sha256 = "12kfpfxs7yl5p2wh33pzixn2b90dqpppiy7p80cg0j1dm9iwi4f1";
    };
  };
  korganizer = {
    version = "21.04.3";
    name = "korganizer-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/korganizer.git";
      rev = "e4b327b647fa22c437fc2dede4b003413eb53bf5";
      sha256 = "05l2qiwv629ppdxy8i6xp64nhd1kh8asp0zx3la06sk1a55h5yaz";
    };
  };
  kpat = {
    version = "21.04.3";
    name = "kpat-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kpat.git";
      rev = "1b5eda109da9fda8c6f808195d572075542d3a23";
      sha256 = "0p9w7y7p2lsny1ix4r5xm96yr95lvx3h9c2i1m78v30h389mmrsz";
    };
  };
  kpimtextedit = {
    version = "21.04.3";
    name = "kpimtextedit-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kpimtextedit.git";
      rev = "38743637a4e7e2c2bd9d82711207af5986d058b1";
      sha256 = "10fa8vvbhrjaf69qyfxwwz1d45gmwr1m09l0h0cmmdgxygz232xb";
    };
  };
  kpkpass = {
    version = "21.04.3";
    name = "kpkpass-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/kpkpass.git";
      rev = "16211f8d8452dddaee585777ed9849a379115db6";
      sha256 = "1y1g2mr3ida5qrih1z0pfis0ayqcx3195a259sb25ilz0g7jjvmi";
    };
  };
  kqtquickcharts = {
    version = "21.04.3";
    name = "kqtquickcharts-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/libraries/kqtquickcharts.git";
      rev = "264af68d6f938961bbf84675e029201d14f67770";
      sha256 = "1rcvahqdj7q12mkhf85bhc6v435a94ibmxah9galjwn81d7xl7hm";
    };
  };
  krdc = {
    version = "21.04.3";
    name = "krdc-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/krdc.git";
      rev = "db18bb3f997026d9f4446394a88b34cfde000d94";
      sha256 = "0i5v7d6wzsb4bfgc3q09sx46a9lv9619pknpzfy0l798cxxi338h";
    };
  };
  kreversi = {
    version = "21.04.3";
    name = "kreversi-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kreversi.git";
      rev = "2abe20ff660b141c04afc411885149826b61e4dc";
      sha256 = "0xjd5aqwpalmcpj5kajyk1znwidikdhhi3asgbm90nvy0jrc50cc";
    };
  };
  krfb = {
    version = "21.04.3";
    name = "krfb-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/network/krfb.git";
      rev = "75bff9d5f96772332055ed48c4a9a0eca6f21b62";
      sha256 = "0d2323r9fjhy5m0z3f39abqv8k1y2wq4jk0h2886s71jkgqja1mi";
    };
  };
  kruler = {
    version = "21.04.3";
    name = "kruler-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/kruler.git";
      rev = "bdd7e07b3f1ec8da2b0df2844d8d94e656aad3a3";
      sha256 = "1ig1ph42wbfpdjxp8vh0p8gagb07ww31bxx8vb0jaih7xj1h7i7w";
    };
  };
  kshisen = {
    version = "21.04.3";
    name = "kshisen-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kshisen.git";
      rev = "448c2ee9e9e89de591e8f0192c6117307e25a546";
      sha256 = "00frxz863ab5vzv83bbnhz715ssjbmxwjjkshz163v4bm35f8fh4";
    };
  };
  ksmtp = {
    version = "21.04.3";
    name = "ksmtp-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/ksmtp.git";
      rev = "59699628120f5861287196387108f4bffa639385";
      sha256 = "08m6qxcxyhd9n88q3ki6gvbab7zw0dwh3h4c5kqws9aa8n887vi8";
    };
  };
  kspaceduel = {
    version = "21.04.3";
    name = "kspaceduel-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/kspaceduel.git";
      rev = "4b314f8be97a1c173b00bf0413c7994fbe1188bf";
      sha256 = "1dxsqxdfxhpiv6pm2ji9qnax1jd6cs9drvsqh1ac6m7kd8mz4g04";
    };
  };
  ksquares = {
    version = "21.04.3";
    name = "ksquares-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/ksquares.git";
      rev = "ffc6ab6ec4e6c25f41c7529c0237394acd464363";
      sha256 = "0r2q7zi72b4facbpbb5qvdkjmvaqsj8zw3pc7d4k6k44zxah6k2b";
    };
  };
  ksudoku = {
    version = "21.04.3";
    name = "ksudoku-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/ksudoku.git";
      rev = "245ababaf7841955d21470f06ee9cd1f7afe697e";
      sha256 = "19gb27wz9sdca6as4b2nqlbxl1fmqwpzlflv7f4lys12ygp2pjr9";
    };
  };
  ksystemlog = {
    version = "21.04.3";
    name = "ksystemlog-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/system/ksystemlog.git";
      rev = "a5b2adcdb6d6ece7844588db2b24c198fd70fe37";
      sha256 = "1vbkgs695kwbdjyf1isc4d95g5ngr7n71h5x7y8z7kpr3k05ii1m";
    };
  };
  kteatime = {
    version = "21.04.3";
    name = "kteatime-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kteatime.git";
      rev = "28209c48e4eae774d123be5b8c28a331bc67da0e";
      sha256 = "0fp9wqab6my43fy8riiacqzm55lgyk64kxfbi4r6y3zz4w74ddwk";
    };
  };
  ktimer = {
    version = "21.04.3";
    name = "ktimer-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/ktimer.git";
      rev = "34951551942c98dee27c28fe53e45a6230737d1e";
      sha256 = "043dcihz6ix45s7pnwd6yfsgw6hx21qjzm4r5ydxv14r87h86pvr";
    };
  };
  ktnef = {
    version = "21.04.3";
    name = "ktnef-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/ktnef.git";
      rev = "a27e7de2f8b9e63f7e913535198f54b3dfadb89f";
      sha256 = "1g91zbrfg9bmi9nq3i7qlwxxpippwzvwyx52vmy6p7dqvapm6v7i";
    };
  };
  ktouch = {
    version = "21.04.3";
    name = "ktouch-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/ktouch.git";
      rev = "62ffc8a6f141ef7e4928adf151b5cfe25ea1f9d5";
      sha256 = "1h7lqmg674mvly698zpgwin52vkjwdb25xggmvvh2hnchm0kj6sf";
    };
  };
  kturtle = {
    version = "21.04.3";
    name = "kturtle-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/kturtle.git";
      rev = "94c8f6dc908a7698ac60b724c61abf840fa35626";
      sha256 = "07w5yqyk3797h55m9maa1xvl3zhxkrcwirmbj0myr7i9q47immb9";
    };
  };
  kwalletmanager = {
    version = "21.04.3";
    name = "kwalletmanager-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/kwalletmanager.git";
      rev = "edd21a2b332dabfe3996a0e9887958c7c93623ae";
      sha256 = "0n2ikxms5kf580ip6h67mh2gcgimmx5w01v83ksdrfv8ymsv56dn";
    };
  };
  kwave = {
    version = "21.04.3";
    name = "kwave-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/kwave.git";
      rev = "a4cf7ac221cdb7cf06439a24b8b0ae78165e5213";
      sha256 = "1pdnmgzqyfp3lyxli3rcw6za5skxwjgxfmmzschmr1cg7qaf6qgq";
    };
  };
  libgravatar = {
    version = "21.04.3";
    name = "libgravatar-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/libgravatar.git";
      rev = "027f657bf2e2334c6c5328242bfc1692c279fa6a";
      sha256 = "1gycyc6amqwl1i7c8m7nawpny80r7d865szdbpv4bbak0dbi2848";
    };
  };
  libkcddb = {
    version = "21.04.3";
    name = "libkcddb-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/multimedia/libkcddb.git";
      rev = "24d5755685d6c79153d59ff265c8ab39cda4024a";
      sha256 = "01idq4y2w79hcxscvdnp3kjajvwyyqdwa3y5axgkvhlb23lrk1ww";
    };
  };
  libkdcraw = {
    version = "21.04.3";
    name = "libkdcraw-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/libkdcraw.git";
      rev = "4af2f689f16e701236575a23c2cbe75e32e775cc";
      sha256 = "11h8bvwyg4qcx6s3jk3c5gjyipzgd6gmwx46gz0kj07lhvn4gmpg";
    };
  };
  libkdegames = {
    version = "21.04.3";
    name = "libkdegames-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/libkdegames.git";
      rev = "9295393e4dc41c552523e70f9ee47c8dcd6745cd";
      sha256 = "1hd3pqqqnm9vqah0sxmgbscsqzk2i7vp990z82kpq1yrpl7d5wvh";
    };
  };
  libkdepim = {
    version = "21.04.3";
    name = "libkdepim-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/libkdepim.git";
      rev = "e2cbe3433b7e0751ae286c79335b0c2578e6b36d";
      sha256 = "178izss1dnlb0azx1x41cmsqffz1w39gdy19hy39w7qjsq6gzvzc";
    };
  };
  libkexiv2 = {
    version = "21.04.3";
    name = "libkexiv2-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/libkexiv2.git";
      rev = "b0e00f4beeb0335eb71b5e94ed44e6ce01e0c9d5";
      sha256 = "0wc16rkhbhblqjily73f54qzz4iikc12ps5vckn4m0g2m86z8396";
    };
  };
  libkgapi = {
    version = "21.04.3";
    name = "libkgapi-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/libkgapi.git";
      rev = "ab3d5483960969120c5ab15927ba5929223eab16";
      sha256 = "1rwc5wvdgka42ka69166mlhqnlr8n8xlk0i7ny3jh8bfzya4y38f";
    };
  };
  libkipi = {
    version = "21.04.3";
    name = "libkipi-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/libkipi.git";
      rev = "eea9c7baca8d565523f556e50726863cecd85286";
      sha256 = "0pxyl938dhcq6373k54x03xzbnag0s90igy780v14bjsmfls6cfa";
    };
  };
  libkleo = {
    version = "21.04.3";
    name = "libkleo-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/libkleo.git";
      rev = "9f79f522c77ff6c16b91bf9dc5fce67e757cf9bf";
      sha256 = "1mj4gmmw3smmb199jb5zkf1wzhqk0h7liw9sgvg73gvmpdqbf3ci";
    };
  };
  libkmahjongg = {
    version = "21.04.3";
    name = "libkmahjongg-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/libkmahjongg.git";
      rev = "559345079c34db0c28c745382806a541f0158640";
      sha256 = "13bkr894arqabrp5s4h9faycl6p3n9mz8n01i29zc6ddlag5hhk1";
    };
  };
  libkomparediff2 = {
    version = "21.04.3";
    name = "libkomparediff2-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/sdk/libkomparediff2.git";
      rev = "9827753bb0900715e5171958fe7dee0446e8b734";
      sha256 = "0aq5i1pg70zn12p1ivf8qa4479n7ws9h2mlibmi0is8cnbqica49";
    };
  };
  libksane = {
    version = "21.04.3";
    name = "libksane-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/libksane.git";
      rev = "cd38e98b200826d62bc4892260cabd1d9757134e";
      sha256 = "0ycbk37ahppl5b74rwxqcvqm3a903q2vm67plv2cvv98xcjrgx97";
    };
  };
  libksieve = {
    version = "21.04.3";
    name = "libksieve-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/libksieve.git";
      rev = "fc1ed60122d3ff7a23255b2a3284c79503e29d7a";
      sha256 = "0psffspnh3drvjwghapmqimb4hycz2gnd8qhfnwib9a68x4hmsfg";
    };
  };
  mailcommon = {
    version = "21.04.3";
    name = "mailcommon-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/mailcommon.git";
      rev = "c8d5a1d681c81fbc044eced1f5646164b46aa10a";
      sha256 = "18iy9q8iix3331xz5bnp6n5f6ymhl2n0dppvm95m6kpyd7r82n30";
    };
  };
  mailimporter = {
    version = "21.04.3";
    name = "mailimporter-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/mailimporter.git";
      rev = "aaac664f18a22860ef57c1b68fce431109f410eb";
      sha256 = "0bdp6cyckm27g8dms4p7s37y6k8c2m76l9y44kq28f5lsxzi7058";
    };
  };
  marble = {
    version = "21.04.3";
    name = "marble-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/marble.git";
      rev = "f08f15fd3784fb078f23750bdf75ffe16eedcfac";
      sha256 = "1yi7ladpqbdcxhznkzvpj8wzi79r800v5vfj2m5yk20kh3n4gyl3";
    };
  };
  mbox-importer = {
    version = "21.04.3";
    name = "mbox-importer-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/mbox-importer.git";
      rev = "fe6e5f9e6931d5b9ece5497385d8a3cb116fd9cf";
      sha256 = "0mjr6azmmfn46s4qkksb3gbvpm96n8f0pzj9mv1fqk09sv4zvgcc";
    };
  };
  messagelib = {
    version = "21.04.3";
    name = "messagelib-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/messagelib.git";
      rev = "1de2247235ce338cf689834f034adccc67b8d15a";
      sha256 = "1wf6y0bqh95msk05qq1vb5w5fdz3j8h299z1qghs9cgwxrqb0r8h";
    };
  };
  minuet = {
    version = "21.04.3";
    name = "minuet-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/minuet.git";
      rev = "a4303ed4587090c554f8569983d823fd203efdab";
      sha256 = "1fdhxgy232nlc0jpslbqnqdgrckfyyjdkg3cb1mpbm49g0x26n1b";
    };
  };
  okular = {
    version = "21.04.3";
    name = "okular-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/okular.git";
      rev = "1d908176608cd9dce49d0fa972eaa7d8ac123eec";
      sha256 = "0b0lfgqqnb26gacwbjja15qgj35xfb6abfk534i25lrbaf47w9ki";
    };
  };
  picmi = {
    version = "21.04.3";
    name = "picmi-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/games/picmi.git";
      rev = "e88c260e44c87aff70d0818f98c453a069b02ce1";
      sha256 = "01kj4rm1yj12hi1nhbs4i2a1lj6rk48dhxckx9msjm8dsaa0k5zs";
    };
  };
  pim-data-exporter = {
    version = "21.04.3";
    name = "pim-data-exporter-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/pim-data-exporter.git";
      rev = "b2205fd4c031f356b45c7a3f878222ca939af1ac";
      sha256 = "0gcbfc91xykn96yiaiiklyr28s3laq9vbgcjjyjax5b24b93kvwg";
    };
  };
  pim-sieve-editor = {
    version = "21.04.3";
    name = "pim-sieve-editor-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/pim-sieve-editor.git";
      rev = "e0cae80bf9c2c5737237c4cd16499a2ca1d0749f";
      sha256 = "1zffvvazsnj7vpy2vi6i5hiahhafw0vcxbyhbsf10rs9nb728rg9";
    };
  };
  pimcommon = {
    version = "21.04.3";
    name = "pimcommon-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/pim/pimcommon.git";
      rev = "6dfebc4ec281b34fb22a9be707b4cafcd32c4513";
      sha256 = "06q86mn0dfq5fic8n8d2qfkd40l8gn6fs6vqkq0gv2y3gqgxmhja";
    };
  };
  print-manager = {
    version = "21.04.3";
    name = "print-manager-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/print-manager.git";
      rev = "927adecfddabd348f168694083cc3d1b007daf75";
      sha256 = "02x5ms7n4pmjbcvv4akznmhdzpk5zafcp78kxm7hb91gcjmm8x7x";
    };
  };
  rocs = {
    version = "21.04.3";
    name = "rocs-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/education/rocs.git";
      rev = "c2e0af1d89f2b2be263326ec93f1ba624418d0ac";
      sha256 = "1xqm6r1pfp6c7paflb6x0z66wcymm411p5gkn2pvppjh70mvrh80";
    };
  };
  spectacle = {
    version = "21.04.3";
    name = "spectacle-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/graphics/spectacle.git";
      rev = "874b5b095b7831b4f9797571e5687ae1c6c2b3ba";
      sha256 = "0fycz9j5zqj7m5ddh3n1zcwx8lq6d76yx9yn6lhd78y6xzrfk951";
    };
  };
  yakuake = {
    version = "21.04.3";
    name = "yakuake-21.04.3";
    src = fetchgit {
      url = "https://invent.kde.org/utilities/yakuake.git";
      rev = "f920d6718a66dcc1d44d76b7a0d7abe3945daeec";
      sha256 = "1apg4fw2z2fly9jl0bgmjzjjyiavq34gi7isw2did0fif1bhsnaz";
    };
  };
}