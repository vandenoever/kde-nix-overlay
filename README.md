# Nix overlay for KDE software

The overlay in this project contains recent KDE packages. It can be used
system-wide in NixOS or per user account.

## What are Nix overlays?

A [Nix overlay](https://nixos.wiki/wiki/Overlays) changes the set of packages in
Nix. They are used to provide more recent or altered versions of packages that
come with Nix.

It can also be used as an alternative to [Nix channels](https://nixos.org/manual/nix/stable/#sec-channels).

## What version of the software is packaged?

The master branch of this Nix overlay packages recent master branches of
KDE software from KDE Frameworks, KDE Plasma and KDE Gear.

A script to update the overlay the current versions of all packages is included.

## Compatibility with NixPkgs and NixOS

This overlay is meant to be used with version 21.05 of NixPkgs and NixOS.
Most of the .nix files in this repository were forked from the directories

 * `pkgs/applications/kde`
 * `pkgs/desktops/plasma-5`
 * `pkgs/development/libraries/kde-frameworks`

from the NixPkgs repository.

## Quick test

To quickly test this overlay by compiling and running `kcalc`, run
```
git clone https://invent.kde.org/vandenoever/kde-nix-overlay.git
cd kde-nix-overlay
nix-shell
```

## Install system-wide on NixOS

As root, run:
```
cd /etc/nixos
git clone https://invent.kde.org/vandenoever/kde-nix-overlay.git
```
and add this to `/etc/nixos/configuration.nix`:
```
nixpkgs.overlays = import ./kde-nix-overlay/overlays.nix;
```

## Update the packages

The script `update_srcs.py` retrieves the latest revisions of all the packages.
This updates the `srcs.nix` files. New versions of the software may require changes in the packaging. The version numbers in the names of the packages are
defined in `update_srcs.py`; check that they are correct before updating.
