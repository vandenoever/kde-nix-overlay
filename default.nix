with import <nixpkgs> { overlays = import ./overlays.nix; };
pkgs.stdenv.mkDerivation {
  name = "example";
  # place the packages you want to try here and run
  # `nix-shell default.nix`
  buildInputs = [ libsForQt5.kcalc ];
}
